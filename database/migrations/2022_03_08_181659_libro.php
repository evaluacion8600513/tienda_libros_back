<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Libro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->increments("id");
            $table->string('titulo');
            $table->integer('num_edicion');
            $table->integer('num_paginas');
            $table->integer('id_categorias');
            $table->date('fecha_publicacion');
            $table->text('sinopsis');
            $table->string('img');
            $table->integer('cantidad');
            $table->integer('id_editoriales');
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros');
    }
}
