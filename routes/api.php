<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EditorialController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\AutorController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// API's Editoriales
Route::get('listEditorial', [EditorialController::class, 'index']);
Route::get('findByLikeEditorial', [EditorialController::class, 'findByLike']);
Route::get('allEditorial', [EditorialController::class, 'combo']);
Route::post('registerEditorial', [EditorialController::class, 'register']);
Route::get('editEditorial/{id?}', [EditorialController::class, 'edit']);
Route::put('updateEditorial/{id}', [EditorialController::class, 'update']);
Route::delete('destroyEditorial/{id}', [EditorialController::class, 'destroy']);

// API's Categorias
Route::get('listCategoria', [CategoriaController::class, 'index']);
Route::get('findByLikeCategoria', [CategoriaController::class, 'findByLike']);
Route::get('allCategoria', [CategoriaController::class, 'combo']);
Route::post('registerCategoria', [CategoriaController::class, 'register']);
Route::get('editCategoria/{id?}', [CategoriaController::class, 'edit']);
Route::put('updateCategoria/{id}', [CategoriaController::class, 'update']);
Route::delete('destroyCategoria/{id}', [CategoriaController::class, 'destroy']);

// API's Autores
Route::get('listAutor', [AutorController::class, 'index']);
Route::get('findByLikeAutor', [AutorController::class, 'findByLike']);
Route::get('allAutor', [AutorController::class, 'combo']);
Route::post('registerAutor', [AutorController::class, 'register']);
Route::get('editAutor/{id?}', [AutorController::class, 'edit']);
Route::put('updateAutor/{id}', [AutorController::class, 'update']);
Route::delete('destroyAutor/{id}', [AutorController::class, 'destroy']);

// API's Libros
Route::get('listLibros', [LibroController::class, 'index']);
Route::get('findByLikeLibros', [LibroController::class, 'findByLike']);
Route::get('allLibros', [LibroController::class, 'combo']);
Route::post('registerLibros', [LibroController::class, 'register']);
Route::get('editLibros/{id?}', [LibroController::class, 'edit']);
Route::put('updateLibros/{idLibro}/{idLibroAutores}', [LibroController::class, 'update']);
Route::delete('destroyLibros/{id}', [LibroController::class, 'destroy']);
Route::get('exportar-excel', [LibroController::class, 'exportarExcel']);
