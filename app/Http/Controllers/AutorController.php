<?php

namespace App\Http\Controllers;

use Validator;
use Exception;
use App\Models\Autor;
use Illuminate\Http\Request;

class AutorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $elementoPorPagina = $request->cant;

        $autor = Autor::paginate($elementoPorPagina);

        return $autor;
    }

    public function findByLike(Request $request)
    {
        $texto = $request->texto;
        $elemoPorPagina = $request->cant;

        $autor = Autor::where('nombre', 'like', '%' . $texto . '%')->paginate($elemoPorPagina);

        return $autor;
    }

    public function combo()
    {
        $autor = Autor::all();

        return $autor;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:100',
            'paterno' => 'required|string|max:150',
            'materno' => 'required|string|max:150',
            'sexo' => 'required|string|max:2',
            'direccion' => 'string|max:255',
            'telefono' => 'string',
            'email' => 'required|string|max:150',
        ], [
            'nombre.required' => 'El Nombre es Obligatorio.',
            'nombre.string' => 'El Nombre debe tener solo letras.',
            'nombre.max' => 'El Nombre tiene que contener 100 caracteres.',
            'paterno.required' => 'El Apellido Paterno es Obligatorio.',
            'paterno.string' => 'El Apellido Paterno debe tener solo letras.',
            'paterno.max' => 'El Apellido Paterno tiene que contener 100 caracteres.',
            'materno.required' => 'El Apellido Materno es Obligatorio.',
            'materno.string' => 'El Apellido Materno debe tener solo letras.',
            'materno.max' => 'El Apellido Materno tiene que contener 100 caracteres.',
            'sexo.required' => 'El Tipo de Sexo es obligatorio.',
            'sexo.string' => 'El Tipo de Sexo debe tener solo letras.',
            'sexo.max' => 'El Tipo de Sexo tiene que contener 2 caracteres.',
            'direccion.string' => 'La Dirección debe tener solo letras.',
            'direccion.max' => 'La Dirección tiene que contener 150 caracteres.',
            'telefono.string' => 'El Teléfono debe ser valido',
            'email.required' => 'El Correo Electrónico es Obligatorio.',
            'email.string' => 'El Correo Electrónico debe tener solo letras.',
            'email.max' => 'El Correo Electrónico tiene que contener 150 caracteres.',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors(), 'estado' => false]);
        }

        $autor = Autor::create([
            'nombre' => $request->nombre,
            'paterno' => $request->paterno,
            'materno' => $request->materno,
            'sexo' => $request->sexo,
            'direccion' => $request->direccion,
            'telefono' => $request->telefono,
            'email' => $request->email,
            'estado' => true
        ]);

        return response()->json(['data' => $autor, 'estado' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '')
    {
        try {
            $autor = Autor::findOrFail($id);
            return response()->json(['data' => $autor]);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Autor] ' . $id) {
                if ($id == '') {
                    $message = 'Introduzca un valor valido de ID a buscar.';
                } else {
                    $message = 'NO se encontro resultado con el ID: ' . $id;
                }
            }
            return response()->json(['message' => $message]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mesage = '';
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required|string|max:100',
                'paterno' => 'required|string|max:150',
                'materno' => 'required|string|max:150',
                'sexo' => 'required|string|max:2',
                'direccion' => 'string|max:255',
                'telefono' => 'string|max:100',
                'email' => 'required|string|max:150',
            ], [
                'nombre.required' => 'El Nombre es Obligatorio.',
                'nombre.string' => 'El Nombre debe tener solo letras.',
                'nombre.max' => 'El Nombre tiene que contener 100 caracteres.',
                'paterno.required' => 'El Apellido Paterno es Obligatorio.',
                'paterno.string' => 'El Apellido Paterno debe tener solo letras.',
                'paterno.max' => 'El Apellido Paterno tiene que contener 100 caracteres.',
                'materno.required' => 'El Apellido Materno es Obligatorio.',
                'materno.string' => 'El Apellido Materno debe tener solo letras.',
                'materno.max' => 'El Apellido Materno tiene que contener 100 caracteres.',
                'sexo.required' => 'El Tipo de Sexo es obligatorio.',
                'sexo.string' => 'El Tipo de Sexo debe tener solo letras.',
                'sexo.max' => 'El Tipo de Sexo tiene que contener 2 caracteres.',
                'direccion.string' => 'La Dirección debe tener solo letras.',
                'direccion.max' => 'La Dirección tiene que contener 150 caracteres.',
                'telefono.string' => 'El Teléfono debe ser valido',
                'telefono.max' => 'El telefono tiene que contener 100 caracteres.',
                'email.required' => 'El Correo Electrónico es Obligatorio.',
                'email.string' => 'El Correo Electrónico debe tener solo letras.',
                'email.max' => 'El Correo Electrónico tiene que contener 150 caracteres.',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            Autor::where('id', '=', $id)->update($request->all());

            $autor = Autor::findOrFail($id);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Autor] ' . $id) {
                if ($id == '') {
                    $mesage = 'El registro a modificar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo modificar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => $autor]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mesage = '';
        try {
            Autor::findOrFail($id);
            Autor::destroy($id);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Autor] ' . $id) {
                if ($id == '') {
                    $mesage = 'El registro a eliminar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo eliminar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => null, 'eliminado' => 'true']);
    }
}
