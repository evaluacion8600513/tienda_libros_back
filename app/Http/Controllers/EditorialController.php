<?php

namespace App\Http\Controllers;

use Validator;
use Exception;
use App\Models\Editorial;
use Illuminate\Http\Request;

class EditorialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $elementoPorPagina = $request->cant;

        $editorial = Editorial::paginate($elementoPorPagina);

        return $editorial;
    }

    public function findByLike(Request $request)
    {
        // dd($request->texto);
        $texto = $request->texto;
        $elementoPorPagina = $request->cant;

        $editorial = Editorial::where('editorial', 'like', '%' . $texto . '%')->paginate($elementoPorPagina);

        return $editorial;
    }

    public function combo()
    {
        $editorial = Editorial::all();

        return $editorial;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'editorial' => 'required|string|max:150',
            'direccion' => 'required|string|max:150',
            'telefono' => 'string|max:100',
            'email' => 'string|max:150',
            'pais' => 'required|string|max:45'
        ], [
            'editorial.required' => 'El Editorial es Obligatorio.',
            'editorial.string' => 'El Editorial debe tener solo letras.',
            'editorial.max' => 'El Editorial tiene que contener 150 caracteres.',
            'direccion.required' => 'La Dirección es obligatorio.',
            'direccion.string' => 'La Dirección debe tener solo letras.',
            'direccion.max' => 'La Dirección tiene que contener 150 caracteres.',
            'telefono.string' => 'El Teléfono debe contener un numero valido',
            'telefono.max' => 'El Teléfono tiene que contener 100 caracteres.',
            'email.string' => 'El Correo Electrónico debe ser valido',
            'email.max' => 'El Correo Electrónico tiene que contener 150 caracteres.',
            'pais.required' => 'El País es Obligatorio.',
            'pais.string' => 'El País debe tener solo letras.',
            'pais.max' => 'El País tiene que contener 45 caracteres.',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors(), 'estado' => false]);
        }

        $editorial = Editorial::create([
            'editorial' => $request->editorial,
            'direccion' => $request->direccion,
            'telefono' => $request->telefono,
            'email' => $request->email,
            'pais' => $request->pais
        ]);

        return response()->json(['data' => $editorial, 'estado' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '')
    {
        try {
            $editorial = Editorial::findOrFail($id);
            return response()->json(['data' => $editorial]);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Editorial] '.$id) {
                if ($id == '') {
                    $message = 'Introduzca un valor valido de ID a buscar.';
                } else {
                    $message = 'NO se encontro resultado con el ID: ' . $id;
                }
            }
            return response()->json(['message' => $message]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mesage = '';
        try {
            $validator = Validator::make($request->all(), [
                'editorial' => 'required|string|max:150',
                'direccion' => 'required|string|max:150',
                'telefono' => 'string|max:100',
                'email' => 'string|max:150',
                'pais' => 'required|string|max:45'
            ], [
                'editorial.required' => 'El Editorial es Obligatorio.',
                'editorial.string' => 'El Editorial debe tener solo letras.',
                'editorial.max' => 'El Editorial tiene que contener 150 caracteres.',
                'direccion.required' => 'La Dirección es obligatorio.',
                'direccion.string' => 'La Dirección debe tener solo letras.',
                'direccion.max' => 'La Dirección tiene que contener 150 caracteres.',
                'telefono.string' => 'El Teléfono debe contener un numero valido',
                'telefono.max' => 'El Teléfono tiene que contener 100 caracteres.',
                'email.string' => 'El Correo Electrónico debe ser valido',
                'email.max' => 'El Correo Electrónico tiene que contener 150 caracteres.',
                'pais.required' => 'El País es Obligatorio.',
                'pais.string' => 'El País debe tener solo letras.',
                'pais.max' => 'El País tiene que contener 45 caracteres.',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            Editorial::where('id', '=', $id)->update($request->all());

            $editorial = Editorial::findOrFail($id);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Editorial] ' . $id) {
                if ($id == '') {
                    $mesage = 'El registro a modificar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo modificar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => $editorial]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mesage = '';
        try {
            Editorial::findOrFail($id);
            Editorial::destroy($id);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Editorial] ' . $id) {
                if ($id == '') {
                    $mesage = 'El registro a eliminar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo eliminar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => null, 'eliminado' => 'true']);
    }
}
