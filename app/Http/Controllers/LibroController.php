<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use App\Models\Autor;
use App\Models\LibrosAutores;
use Validator;
use Exception;
use Illuminate\Http\Request;
use App\Exports\MiExportacion;
use Maatwebsite\Excel\Facades\Excel;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $elementoPorPagina = $request->cant;

        $libro = LibrosAutores::with('libros')->with('autores')->paginate($elementoPorPagina);

        return $libro;
    }

    public function findByLike(Request $request)
    {
        $autores = $request->autores;
        $publicacion = $request->publicacion;
        $editorial = $request->editorial;
        $categoria = $request->categoria;
        $elementoPorPagina = $request->cant;

        $libro = LibrosAutores::
            with('libros')
            ->with('autores')
            ->whereHas('autores', function ($query) use ($autores) {
                $query->orWhere('nombre', 'like', '%' . $autores . '%');
            })
            ->whereHas('libros', function ($query) use ($publicacion, $editorial, $categoria) {
                $query->orWhere('fecha_publicacion', '=', $publicacion)
                    ->orWhere('id_editoriales', '=', $editorial)
                    ->orWhere('id_categorias', '=', $categoria);
            })
            ->paginate($elementoPorPagina);

        return $libro;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'titulo' => 'required|string|max:150',
            'num_edicion' => 'required|integer',
            'num_paginas' => 'integer',
            'id_categorias' => 'required|integer',
            'fecha_publicacion' => 'required|max:45',
            'sinopsis' => 'string',
            'img' => 'required|string',
            'cantidad' => 'required|integer',
            'id_editoriales' => 'required|integer',
            'autores' => 'required|integer',
        ], [
            'titulo.required' => 'El Titulo es Obligatorio.',
            'titulo.string' => 'El Titulo debe tener solo letras.',
            'titulo.max' => 'El Titulo tiene que contener 150 caracteres.',
            'num_edicion.required' => 'El Número de Edición es obligatorio.',
            'num_edicion.integer' => 'El Número de Edición deben ser números.',
            'num_paginas.integer' => 'El Número de Páginas deben ser números.',
            'id_categorias.required' => 'La Categoria es Obligatorio.',
            'id_categorias.integer' => 'Seleecione una Categoria valida.',
            'fecha_publicacion.required' => 'La Fecha de Publicación es Obligartorio.',
            'fecha_publicacion.max' => 'La Fecha de Publicación debe ser una fecha valida.',
            'sinopsis.string' => 'El resumen debe ser letras.',
            'img.required' => 'La Imagen esa Obligatorio.',
            'img.string' => 'Selecciona una Imagen valida.',
            'cantidad.required' => 'La Cantida de Existencias es Obligatorio.',
            'cantidad.integer' => 'La Cantida de Existencias debe ser numeros.',
            'id_editoriales.required' => 'La Editorial es Obligatorio.',
            'id_editoriales.integer' => 'Seleccione una EditorialLa valida.',
            'autores.required' => 'Los Autores es Obligatorio.',
            'autores.integer' => 'Seleccione Autores de la lista.',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors(), 'estado' => false]);
        }

        $libro = Libro::create([
            'titulo' => $request->titulo,
            'num_edicion' => $request->num_edicion,
            'num_paginas' => $request->num_paginas,
            'id_categorias' => $request->id_categorias,
            'fecha_publicacion' => $request->fecha_publicacion,
            'sinopsis' => $request->sinopsis,
            'img' => $request->img,
            'cantidad' => $request->cantidad,
            'id_editoriales' => $request->id_editoriales,
        ]);

        $id = $libro->id;

        $librosAutores = LibrosAutores::create([
            'id_libros' => $id,
            'id_autores' => $request->autores,
        ]);

        return response()->json(['data' => $libro, 'estado' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '')
    {
        try {
            $librosAutores = LibrosAutores::findOrFail($id);

            // $libro = Libro::findOrFail(($librosAutores->id_libros))->with('editoriales')->with('categorias')->get();
            $libro = Libro::where('id', '=', $librosAutores->id_libros)->with('editoriales')->with('categorias')->get();
            $autores = Autor::findOrFail(($librosAutores->id_autores));

            return response()->json(['libro' => $libro, 'autores' => $autores, 'librosAutores' => $librosAutores]);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\LibrosAutores] ' . $id) {
                if ($id == '') {
                    $message = 'Introduzca un valor valido de ID a buscar.';
                } else {
                    $message = 'NO se encontro resultado con el ID: ' . $id;
                }
            }
            return response()->json(['message' => $message]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idLibro, $idLibroAutores)
    {
        // dd($idLibroAutores);
        $mesage = '';
        try {
            $validator = Validator::make($request->all(), [
                'titulo' => 'required|string|max:150',
                'num_edicion' => 'required|integer',
                'num_paginas' => 'integer',
                'id_categorias' => 'required|integer',
                'fecha_publicacion' => 'required|max:45',
                'sinopsis' => 'string',
                'img' => 'required|string',
                'cantidad' => 'required|integer',
                'id_editoriales' => 'required|integer',
                'autores' => 'required|integer',
            ], [
                'titulo.required' => 'El Titulo es Obligatorio.',
                'titulo.string' => 'El Titulo debe tener solo letras.',
                'titulo.max' => 'El Titulo tiene que contener 150 caracteres.',
                'num_edicion.required' => 'El Número de Edición es obligatorio.',
                'num_edicion.integer' => 'El Número de Edición deben ser números.',
                'num_paginas.integer' => 'El Número de Páginas deben ser números.',
                'id_categorias.required' => 'La Categoria es Obligatorio.',
                'id_categorias.integer' => 'Seleecione una Categoria valida.',
                'fecha_publicacion.required' => 'La Fecha de Publicación es Obligartorio.',
                'fecha_publicacion.max' => 'La Fecha de Publicación debe ser una fecha valida.',
                'sinopsis.string' => 'El resumen debe ser letras.',
                'img.required' => 'La Imagen esa Obligatorio.',
                'img.string' => 'Selecciona una Imagen valida.',
                'cantidad.required' => 'La Cantida de Existencias es Obligatorio.',
                'cantidad.integer' => 'La Cantida de Existencias debe ser numeros.',
                'id_editoriales.required' => 'La Editorial es Obligatorio.',
                'id_editoriales.integer' => 'Seleccione una EditorialLa valida.',
                'autores.required' => 'Los Autores es Obligatorio.',
                'autores.integer' => 'Seleccione Autores de la lista.',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            Libro::where('id', '=', $idLibro)->update([
                'titulo' => $request->titulo,
                'num_edicion' => $request->num_edicion,
                'num_paginas' => $request->num_paginas,
                'id_categorias' => $request->id_categorias,
                'fecha_publicacion' => $request->fecha_publicacion,
                'sinopsis' => $request->sinopsis,
                'img' => $request->img,
                'cantidad' => $request->cantidad,
                'id_editoriales' => $request->id_editoriales,
            ]);

            LibrosAutores::where('id', '=', $idLibroAutores)->update([
                'id_autores' => $request->autores,
            ]);

            $libros = LibrosAutores::where('id', '=', $idLibroAutores)->with('libros')->with('autores')->get();
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Libro] ' . $idLibro) {
                if ($id == '') {
                    $mesage = 'El registro a modificar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo modificar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => $libros]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idLibro, $idLibroAutores)
    {
        $mesage = '';
        try {
            Libro::findOrFail($idLibro);
            Libro::destroy($idLibro);

            LibrosAutores::findOrFail($idLibroAutores);
            LibrosAutores::destroy($idLibroAutores);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Libro] ' . $idLibro) {
                if ($id == '') {
                    $mesage = 'El registro a eliminar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo eliminar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => null, 'eliminado' => 'true']);
    }

    public function exportarExcel()
    {
        $export = new MiExportacion();
        $file = Excel::download($export, 'miarchivo.xlsx')->getFile();

        return response($file, 200)
            ->header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            ->header('Content-Disposition', 'attachment; filename="miarchivo.xlsx"');

    }
}
