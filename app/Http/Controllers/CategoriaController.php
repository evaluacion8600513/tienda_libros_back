<?php

namespace App\Http\Controllers;

use Validator;
use Exception;
use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $elementoPorPagina = $request->cant;

        $categoria = Categoria::paginate($elementoPorPagina);

        return $categoria;
    }

    public function findByLike(Request $request)
    {
        $texto = $request->texto;
        $elemoPorPagina = $request->cant;

        $categoria = Categoria::where('categoria', 'like', '%' . $texto . '%')->paginate($elemoPorPagina);

        return $categoria;
    }

    public function combo()
    {
        $categoria = Categoria::all();

        return $categoria;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'categoria' => 'required|string|max:80',
        ], [
            'categoria.required' => 'La Categoria es Obligatorio.',
            'categoria.string' => 'La Categoria debe tener solo letras.',
            'categoria.max' => 'La Categoria tiene que contener 80 caracteres.',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors(), 'estado' => false]);
        }

        $categoria = Categoria::create([
            'categoria' => $request->categoria,
            'estado' => true
        ]);

        return response()->json(['data' => $categoria, 'estado' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = '')
    {
        $message = "";
        try {
            $categoria = Categoria::findOrFail($id);
            return response()->json(['data' => $categoria]);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Categoria] ' . $id) {
                if ($id == '') {
                    $message = 'Introduzca un valor valido de ID a buscar.';
                } else {
                    $message = 'NO se encontro resultado con el ID: ' . $id;
                }
            }
            return response()->json(['message' => $message]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mesage = '';
        try {
            $validator = Validator::make($request->all(), [
                'categoria' => 'required|string|max:80',
            ], [
                'categoria.required' => 'La Categoria es Obligatorio.',
                'categoria.string' => 'La Categoria debe tener solo letras.',
                'categoria.max' => 'La Categoria tiene que contener 80 caracteres.',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            Categoria::where('id', '=', $id)->update($request->all());

            $categoria = Categoria::findOrFail($id);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Categoria] ' . $id) {
                if ($id == '') {
                    $mesage = 'El registro a modificar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo modificar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => $categoria]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mesage = '';
        try {
            Categoria::findOrFail($id);
            Categoria::destroy($id);
        } catch (Exception $exception) {
            if ($exception->getMessage() == 'No query results for model [App\Models\Categoria] ' . $id) {
                if ($id == '') {
                    $mesage = 'El registro a eliminar no fue encontrado. Verifique los datos enviados.';
                } else {
                    $mesage = 'No se pudo eliminar, SI el error persiste comuniquese con el Administrador del Sistema.';
                }
            }
            return response()->json(['message' => $mesage]);
        }
        return response()->json(['data' => null, 'eliminado' => 'true']);
    }
}
