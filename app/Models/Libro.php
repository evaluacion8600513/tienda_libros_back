<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;

    protected $table = 'libros';

    protected $fillable = ['titulo', 'num_edicion', 'num_paginas', 'id_categorias', 'fecha_publicacion', 'sinopsis', 'img', 'cantidad', 'id_editoriales'];

    public function categorias()
    {
        return $this->hasMany(Categoria::class, 'id', 'id_categorias');
    }

    public function editoriales()
    {
        return $this->hasMany(Editorial::class, 'id', 'id_editoriales');
    }
}
