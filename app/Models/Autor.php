<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    use HasFactory;

    protected $table = 'autores';

    protected $fillable = ['nombre', 'paterno', 'materno', 'sexo', 'direccion', 'telefono', 'email', 'estado'];

    public function libros()
    {
        return $this->belongsTo(LibrosAutores::class);
    }
}
