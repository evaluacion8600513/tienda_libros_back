<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibrosAutores extends Model
{
    use HasFactory;

    protected $table = 'librosautores';

    public $timestamps = false;

    protected $fillable = ['id_libros', 'id_autores'];

    public function libros()
    {
        return $this->hasMany(Libro::class, 'id', 'id_libros');
    }

    public function autores()
    {
        return $this->hasMany(Autor::class, 'id', 'id_autores');
    }
}
