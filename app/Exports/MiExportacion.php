<?php
namespace App\Exports;

use App\Models\Libro;
use Maatwebsite\Excel\Concerns\FromCollection;

class MiExportacion implements FromCollection
{
    public function collection()
    {
        return Libro::all();
    }
}